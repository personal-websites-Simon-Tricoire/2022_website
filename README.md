# Personal website (2022-Now)

<figure>
  <img src="Design/Website_1.png" width="500">
</figure>

<br>

This a website I made in early 2022 as a new version of my previous website.
I decided to rebuild it entirely because I wanted a simpler, more epurate design,
which link all my different medias ([Itchio](https://lumbirbwut.itch.io/), [Linkedin](https://www.linkedin.com/in/simon-tricoire-3b34bb8a/), this [Gitlab](https://gitlab.com/stricoire)).
I also wanted a website easier to actualise.

Finaly I tried to push further the ecologic aspect of it :
  - There are no images on it
  - My Css contained less than 128 lines (compared to 608 of my previous one)
  - My Website only weight 7,1ko (compared to 1,8Mo of my previous one)

I did all the design and programmation.

### Inspirations

I got inspired by several website including [Low tech magazine](https://solar.lowtechmagazine.com/),
[Timothee Goguely](https://timothee.goguely.com/), [Marc Seitz](https://mfts.io/), [Jeremy Thomas](https://jgthms.com/web-design-in-4-minutes/) and [Mark wonnacott](https://kool.tools/speculations/#about).

Big shoutout to [Timothee Goguely](https://timothee.goguely.com/) who helped me even more to improve this website, making it even cleaner and accessible than before. 

### Audit tools

I used in particular :
- [WebsiteCarbon](https://www.websitecarbon.com/) to analyse the impact of my website on the planet,
- [EcoIndex Score](http://www.ecoindex.fr/) to analyse its environmental footprint and performance
- [Lighthouse](https://web.dev/measure/) and [Pingdom](https://tools.pingdom.com/#5f910a74ae800000) for the overall performance
- [WAVE](https://addons.mozilla.org/fr/firefox/addon/wave-accessibility-tool/) to analyse its accessibility for website accessibility
- [Nu Html Checker](https://validator.w3.org/nu/) to check my HTML
